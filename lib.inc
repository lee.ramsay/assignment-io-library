global _start

section .data
hello: db "hello, world"
newline_char: db 10

section .data
buffer: times 256 db 0

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit:
	mov rax, rdi	 
	syscall 


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
	.counter:
		cmp byte [rdi+rax], 0
		je .end
		inc rax
		jmp .counter
	.end: ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	xor rax, rax

	call string_length
	
	mov rdx, rax
	mov rax, 1
	
	mov rsi, rdi
	mov rdi, 1
	
	syscall
	ret


; Принимает код символа и выводит его в stdout
print_char:
	xor rax, rax

	push rdi 	; push code to stack
	mov rsi, rsp
	mov rdx, 1	; number of bytes (1)
	mov rax, 1	; syscall for write
	
	mov rdi, 1
	
	syscall
	pop rdi
	ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	xor rax, rax
    
	mov rdx, 1
	mov rax, 1
	
	mov rsi, newline_char
	mov rdi, 1
	
	syscall
	ret


;DONE
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	xor rax, rax
    
	mov r10, 10
	mov rax, rdi
	mov rdi, rsp
	sub rsp, 21
	dec rdi
	mov byte[rdi], 0
	
	.loop:
		xor rdx, rdx
		div r10
		add rdx, '0'
		
		dec rdi
		mov [rdi], dl
		
		test rax, rax
		jnz .loop
		
	.sout:
		call print_string
		add rsp, 21
		ret
	


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	xor rax, rax
    
	cmp rdi, 0
	jl .print_min
	jmp .end
	
	.print_min:
		push rdi
		mov rdi, '-'
		call print_char
		pop rdi
		neg rdi
	.end:
		jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	xor rdx,rdx
	xor rax, rax
    

    .checking:
        cmp byte[rdi+rdx],0
        jz .next
        xor al,al
        xor cl,cl
        mov al,byte[rdi+rdx]
        mov cl,byte[rsi+rdx]
        inc rdx
        cmp al,cl
        je .checking
        jmp .unequal
    .next:
        cmp byte[rsi+rdx],0
        jz .equal
    .unequal:
        xor rax,rax
        ret
    .equal:
        mov rax,1
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	dec rsp
	xor rax, rax
	xor rdi, rdi
	mov rdx, 1
	mov rsi, rsp
	syscall
	mov rax, [rsp]
	inc rsp
	ret 


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	read_word:
	mov r8,rsi 
	mov r9,rdi 
	xor r10,r10
	xor rdi,rdi
	mov rdx,1 
	.skip:
		xor rax,rax 
		mov rsi,buffer
		syscall
		test al,al 
		jz .finall 
		cmp byte[buffer],0x20 
		je .skip 
		cmp byte[buffer],0x9 
		je .skip 
		cmp byte[buffer],0xA 
		je .skip  
		inc r10 
	.read:
		xor rax, rax 
		lea rsi,[buffer+r10] 
		syscall
		mov cl,byte[buffer+r10]
		test cl,cl
		jz .finall 
		cmp cl,0x20 
		je .finall 
		cmp cl,0x9 
		je .finall 
		cmp cl,0xA 
		je .finall 
		cmp r8,r10 
		jbe .exi 
		inc r10 
		jmp .read 
	.finall:
		mov byte[buffer+r10],0 
		mov rdi,buffer
		mov rsi,r9
		mov rdx,r8
		call string_copy 
		mov rdx,r10 
		mov rax,r9 
		ret
	.exi:
		xor rdx,r8 
		xor rax,rax
		ret

 


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rax, rax
	xor rsi,rsi
	xor rax,rax
	xor r9,r9
	mov r8,10
.nums:
	mov r9b,byte[rdi+rsi]
    cmp r9b, '0'
    jb .end
    cmp r9b, '9'
    ja .end
	mul r8
    sub r9b,'0'
    add rax,r9
    inc rsi
    jmp .nums
.end:
    mov rdx,rsi
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	xor rax, rax
    
	xor rdx, rdx
	xor rsi, rsi
	mov r9b, byte[rdi]
	
	cmp r9b, '-'
	je .minus
	
	call parse_uint
	ret
	
	.minus:
		inc rdi
		mov r9b, byte[rdi+rsi]
		cmp r9b, 0x20
		je .end
		cmp r9b, 0x9
		je .end
		cmp r9b, 0x10
		je .end
		call parse_uint
		test rdx, rdx
		jz .end
		inc rdx
		neg rax
		ret
	.end:
		ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	push rdi
	
	.copy:
		xor rcx, rcx
		mov cl, [rdi]
		mov [rsi], cl
		inc rsi
		inc rdi
		test rcx, rcx
		jnz .copy
		pop rdi
		call string_length
		inc rax
		cmp rdx, rax
		jae .end
		xor rax, rax
	.end:
		ret

